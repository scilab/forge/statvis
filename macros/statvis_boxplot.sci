function statvis_boxplot(varargin)
// Draw a box-and-whiskers plot for data provided as column vectors.
//
// Calling Sequence
//  statvis_boxplot(y1[,y2,...])
//  statvis_boxplot(y1[,y2,...],G)
//  statvis_boxplot(Y_list)
//
// Parameters
//  y1,y2,... : vectors and/or matrixes with data as column vectors (of varying length) 
//  G: string vector defines the column by strings
//  Y_list : list of vectors and/or matrixes with data as column vectors (of varying length)
//
// Description
//  A boxplot (also known as a box-and-whisker plot is a way of 
//  graphically depicting groups of numerical data through their five-number summaries 
//  (the smallest observation, lower quartile (Q1), median, upper quartile (Q3), and 
//  largest observation). A boxplot may also indicate which observations, if any, 
//  might be considered outliers. The boxplot was invented in 1977 by the 
//  American statistician John Tukey.
//
//  For each data series a box is drawn to indicate the position of the lower and upper 
//  quartile of the data. The box has a centre line indicating the median of the data.
//  Straigh center lines (the whiskers) above and below the box indicates the 
//  maximum and minimum values in the data set (except for outliers). 
//  Outliers are defined as any points larger than Q3 + 1.5*IQR or lower then
//  Q1 - 1.5*IQR, where IQR is the inter quartile range defined as IQR=Q3-Q1.
//  Outliers are plotted as individual '*'. 
//
//  Boxplots can be useful to display differences between populations without making 
//  any assumptions of the underlying statistical distribution. The spacings between 
//  the different parts of the box help indicate the degree of dispersion (spread) and 
//  skewness in the data, and identify outliers. 
//
// Examples
//  statvis_boxplot([rand(10,5);5*rand(5,5)-2.5]) 
//  statvis_boxplot() // demo and help
//
// Bibliography
//  http://en.wikipedia.org/wiki/Box_plot
//
// Authors
//  T. Pettersen (2008)

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

if argn(2)==0 then
  statvis_boxplot([rand(10,5);5*rand(5,5)-2.5]);
  xtitle('boxplot() draws a Box and Whiskers plot','Data series no.','Data range');
  help statvis_boxplot
  return;
end

X=0; wd=1/4; style='b-'; change_column_names=%f;
set(gcf(),'immediate_drawing','off');

  if type(varargin(1))==15 then
   N=length(varargin(1));
  else
    N=argn(2);
  end;


for i=1:N,
  if type(varargin(1))==15 then
    Y=varargin(1)(i);
  else
    Y=varargin(i);
  end;

  if type(Y)==10 & i==argn(2) then 
    change_column_names=%t;
    column_names=Y;

  else
    if size(Y,'r')==1 then error('Input data should be given as column vectors'); end
    for j=1:size(Y,'c'),
      X=X+1;
      Q=quart(Y(:,j)); Min=min(Y(:,j)); Max=max(Y(:,j));
      plot([X-wd;X+wd;X+wd;X-wd;X-wd;],[Q(1);Q(1);Q(3);Q(3);Q(1)],style);  // the quartile box
      plot([X-wd;X+wd],[Q(2);Q(2)],'r'); // the median
      IQR=Q(3)-Q(1);  // the inter quartile range = Q3 - Q1
      outliers=Y(find(Y(:,j)>Q(3)+1.5*IQR),j);
      outliers=[outliers;Y(find(Y(:,j)<Q(1)-1.5*IQR),j)];
      if ~isempty(outliers) then 
	plot(X*ones(outliers),outliers,'r+'); // the outliers - if any
	large=find(outliers>Q(3));
	if large then
	  whiskers=max(Y(find(Y(:,j)<min(outliers(large))),j));
	else
	  whiskers=Max;
	end
	small=find(outliers<Q(1));
	if small then
	  whiskers=[min(Y(find(Y(:,j)>max(outliers(small))),j));whiskers];
	else
	  whiskers=[Min;whiskers];
	end
      else
	whiskers=[Min;Max];
      end
      plot([X;X],[whiskers(1);Q(1)],'k--');   // the lower whisker
      plot([X-wd/2;X+wd/2], [whiskers(1);whiskers(1)],'k');

      plot([X;X],[whiskers(2);Q(3)],'k--');   // the upper whiskers
      plot([X-wd/2;X+wd/2], [whiskers(2);whiskers(2)],'k');
    end
  end
end

a=gca();
a.data_bounds(:,1)=[0;X+1];
x_ticks=a.x_ticks;
x_ticks(2)=(1:X)';
if change_column_names & max(size(column_names))==X then
  x_ticks(3)=column_names(:);
else
   x_ticks(3)=string((1:X)');
   xlabel("Column Number");
end;
a.x_ticks=x_ticks;
a.sub_ticks(1)=0;

ylabel("Values");

set(gcf(),'immediate_drawing','on');
endfunction
