// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA - Allan CORNET
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()

demopath = get_absolute_file_path("statvis.dem.gateway.sce");

subdemolist=['boxplot demo', 'boxplot_demo.sce'];


subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
